//
//  ViewSnapViewController.swift
//  Snapchat
//
//  Created by Jose Ma. P. Alvarado on 10/04/2017.
//  Copyright © 2017 1010. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage


class ViewSnapViewController: UIViewController {

    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    // Declare a placeholder to pass on values from source viewcontroller
    var snap = Snap()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        label.text = snap.descrip
        
        // Standard code for downloading image from firebase using SDWebImage cocoapods. Make sure to include it in the Podfile then do a "pod install" in the terminal (Make sure you are in the root folder of your project before pod install)
        imageView.sd_setImage(with: URL(string: snap.imageURL))
        
    }

    // Standard function for moving out of the viewcontroller via Navigation Controller
    override func viewWillDisappear(_ animated: Bool) {
        
        // Standard code for deleting value in Firebase database
        FIRDatabase.database().reference().child("users").child(FIRAuth.auth()!.currentUser!.uid).child("snaps").child(snap.key).removeValue()
    
        
        FIRStorage.storage().reference().child("images").child("\(snap.uuid).png").delete { (error) in
            print("File deleted")
        }
        
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
