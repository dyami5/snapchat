//
//  PictureViewController.swift
//  Snapchat
//
//  Created by Jose Ma. P. Alvarado on 04/04/2017.
//  Copyright © 2017 1010. All rights reserved.
//

import UIKit
import FirebaseStorage

class PictureViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    // Standard code needed to access photos/camera in the phone: requires setup of delegates in the class "UIImagePickerControllerDelegate, UINavigationControllerDelegate" and another property in info.plist "Privacy - Photo Library Usage"
    var imagePicker = UIImagePickerController()
    
    // Needed for deleting specific file in FirebaseStorage
    var uuid = NSUUID().uuidString
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Standard code needed for imagePickerController to work
        imagePicker.delegate = self
        
        nextButton.isEnabled = false
    }

    
    // Standard function needed when using camera/photo library
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        imageView.image = image
        
        // Code to remove background color in UIImage 
        imageView.backgroundColor = UIColor.clear
        
        nextButton.isEnabled = true
        
        // Standard code to dismiss the photo library screen. This is required everytime you call the photo library.
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func cameraTapped(_ sender: Any) {
        
        // Standard code to access the photo library - you can change .camera when uploading to actual device (don't forget to add "Privacy - Camera..." in Info.plist)
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = false
        
        // Standard code to present the photo library of the phone
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @IBAction func nextTapped(_ sender: Any) {
        
        nextButton.isEnabled = false
        
        
        let imagesFolder = FIRStorage.storage().reference().child("images")
        
        // Use this code to covert image to jpeg to make it a smaller file
        let imageData = UIImageJPEGRepresentation(imageView.image!, 0.1)!
        
        // NSUUID().uuidString allows swift to create a unique name for the file
        imagesFolder.child("\(uuid).png").put(imageData, metadata: nil) { (metadata, error) in
            print("We tried to upload.")
            if error != nil {
                
                print("We had an error:\(error)")
                
            } else {
                
                // metadata?.downloadURL() -- allows you to retrieve the url of the image
                
                self.performSegue(withIdentifier: "selectUserSegue", sender: metadata?.downloadURL()!.absoluteString)
                
            }
            
        }
        
    }
    
    // Standard code to pass fields to another segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let nextVC = segue.destination as! SelectUserViewController
        
        nextVC.imageURL = sender as! String
        nextVC.descrip = descriptionTextField.text!
        nextVC.uuid = uuid
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}
