//
//  SnapsViewController.swift
//  Snapchat
//
//  Created by Jose Ma. P. Alvarado on 03/04/2017.
//  Copyright © 2017 1010. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class SnapsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {


    @IBOutlet weak var tableView: UITableView!
    
    
    // Declare the array of the "Snap" class object (Models) - Requires to create the Snap swift file under Models as a placeholder for the data
    var snaps : [Snap] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Standard code for tableView
        tableView.dataSource = self
        tableView.delegate = self
        
        // Standard code to Pull data from Firebase Database
        FIRDatabase.database().reference().child("users").child(FIRAuth.auth()!.currentUser!.uid).child("snaps").observe(FIRDataEventType.childAdded, with: { (snapshot) in
            print(snapshot)
            
            let snap = Snap()
            
            snap.imageURL = (snapshot.value as! NSDictionary)["imageURL"] as! String
            snap.from = (snapshot.value as! NSDictionary)["from"] as! String
            snap.descrip = (snapshot.value as! NSDictionary)["description"] as! String
            snap.key = snapshot.key
            snap.uuid = (snapshot.value as! NSDictionary)["uuid"] as! String
            
            
            self.snaps.append(snap)
            
            self.tableView.reloadData()
            
        })

        // Same code as above only difference is FIRDataEventType.childRemoved to remove from tableView
        FIRDatabase.database().reference().child("users").child(FIRAuth.auth()!.currentUser!.uid).child("snaps").observe(FIRDataEventType.childRemoved, with: { (snapshot) in
            print(snapshot)
            
            // Used in for loop to assign index for the array
            var index = 0
            
            for snap in self.snaps{
            
                if snap.key == snapshot.key {
                
                    self.snaps.remove(at: index)
                    
                }
                
                index += 1
            
            }
            
            self.tableView.reloadData()
            
        })
        
        
    }

    
    @IBAction func logoutTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if snaps.count == 0 {
            return 1
        } else {
            return snaps.count
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        if snaps.count == 0 {
            
            cell.textLabel?.text = "You have no snaps 😞"
            
        } else {
            
            let snap = snaps[indexPath.row]
            
            cell.textLabel?.text = snap.from
            
        }
        
        
        return cell
        
    }
    
    // Standard function when selecting data from a tableview
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let snap = snaps[indexPath.row]
        
        // You need to name the identifier of the segue connection for the viewcontroller
        performSegue(withIdentifier: "viewsnapsegue", sender: snap)

    }

    // Standard function when passing on values to a destination viewcontroller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "viewsnapsegue" {
            
            let nextVC = segue.destination as! ViewSnapViewController
            nextVC.snap = sender as! Snap
            
        }
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
